import 'package:edenred/blocs/books/books_bloc.dart';
import 'package:edenred/blocs/books/cart_bloc.dart';
import 'package:edenred/network/models/book.dart';
import 'package:edenred/screens/loader.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BookListWidget extends StatefulWidget {
  const BookListWidget({Key? key}) : super(key: key);

  @override
  _BookListWidgetState createState() => _BookListWidgetState();
}

class _BookListWidgetState extends State<BookListWidget> {
  final BooksBloc _booksBloc = BooksBloc();
  final CartBloc _cartBloc = CartBloc();

  @override
  void initState() {
    _booksBloc.add(GetBooks());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("La bibliothèque d'Henri Potier"),
        ),
        body: BlocProvider(
          create: (_) => _cartBloc,
          child: BlocBuilder<CartBloc, CartState>(
            builder: (cartContext, cartState) {
              if (cartState is CartTotal) {
                return Center(
                  child: Text("Cart total is " + cartState.total.toString()),
                );
              }

              return Stack(
                children: [
                  _booksBuilder(),
                  Align(
                    alignment: Alignment.topRight,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: FloatingActionButton(
                        key: const Key('floatingActionButton'),
                        child: Text("${_cartBloc.isbns.length}"),
                        onPressed: () => cartContext
                            .read<CartBloc>()
                            .add(CartSummary(allBooks: _booksBloc.books)),
                      ),
                    ),
                  ),
                ],
              );
            },
          ),
        ));
  }

  Widget _booksBuilder() {
    return BlocProvider(
      create: (_) => _booksBloc,
      child: BlocBuilder<BooksBloc, BooksState>(
        builder: (context, state) {
          if (state is BooksLoading) {
            return const LoaderWidget();
          }

          if (state is BooksLoaded) {
            return BooksWidget(books: state.books);
          }

          return const Center(
            child: Text("Error Occured"),
          );
        },
      ),
    );
  }
}

class BooksWidget extends StatelessWidget {
  final List<Book> books;
  const BooksWidget({Key? key, required this.books}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: books.length,
      itemBuilder: (BuildContext context, int index) {
        return Card(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Container(
                      width: 50,
                      child: Image.network(books[index].cover),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Text(books[index].title),
                          IconButton(
                            onPressed: () => context
                                .read<CartBloc>()
                                .add(AddToCart(isbn: books[index].isbn)),
                            icon: const Icon(
                              Icons.add_shopping_cart,
                              color: Colors.blue,
                              size: 30.0,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(books[index].synopsis[0]),
              ),
            ],
          ),
          // ListTile(
          // title: Text(books[index].title),
          // subtitle:
          // leading:
          // trailing: IconButton(onPressed: onPressed, icon: Icon),
          // ),
        );
      },
    );
  }
}
