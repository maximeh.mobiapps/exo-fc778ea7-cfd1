import 'package:decimal/decimal.dart';
import 'package:dio/dio.dart';
import 'package:edenred/network/client.dart';
import 'package:edenred/network/models/book.dart';
import 'package:edenred/network/models/commercial_offer.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

/* EVENTS */
abstract class CartEvent {}

class AddToCart extends CartEvent {
  final String isbn;

  AddToCart({required this.isbn});
}

class CartSummary extends CartEvent {
  final List<Book> allBooks;

  CartSummary({required this.allBooks});
}

/* STATES */

abstract class CartState extends Equatable {
  const CartState();

  @override
  List<Object?> get props => [];
}

class CartEmpty extends CartState {}

class CartBookAdded extends CartState {
  final String isbn;

  const CartBookAdded({required this.isbn});

  @override
  List<Object?> get props => [isbn];
}

class CartTotal extends CartState {
  final Decimal total;

  const CartTotal({required this.total});

  @override
  List<Object?> get props => [total];
}

class CartError extends CartState {}

/* BLOC */

class CartBloc extends Bloc<CartEvent, CartState> {
  final isbns = <String>[];

  CartBloc() : super(CartEmpty()) {
    on<AddToCart>((event, emit) async {
      if (!isbns.contains(event.isbn)) {
        isbns.add(event.isbn);
        emit(CartBookAdded(isbn: event.isbn));
      }
    });

    on<CartSummary>((event, emit) async {
      final result = await _fetchBestOffer(event.allBooks);
      emit(CartTotal(total: result));
    });
  }

  Future<Decimal> _fetchBestOffer(List<Book> books) async {
    final dio = Dio(); // Provide a dio instance
    final client = RestClient(dio);

    final Decimal rawTotal = Decimal.fromInt(books
        .where((book) => isbns.contains(book.isbn))
        .map((book) => book.price)
        .reduce((value, element) => value + element));
    Decimal bestOffer = rawTotal;

    final offers = await client.getCommercialOffers(isbns.join(","));
    for (CommercialOffer offer in offers.offers) {
      if (offer.type == OfferType.percentage) {
        final percentage = _getPercentageTotal(rawTotal, offer.value);
        print("Percentage:" + percentage.toString());
        if (percentage < bestOffer) {
          bestOffer = percentage;
        }
      } else if (offer.type == OfferType.minus) {
        final minus = rawTotal - Decimal.fromInt(offer.value);
        print("Minus:" + minus.toString());
        if (minus < bestOffer) {
          bestOffer = minus;
        }
      } else if (offer.type == OfferType.slice) {
        final modulo =
            (rawTotal / Decimal.fromInt(offer.sliceValue ?? 9999)).floor();
        final slice = rawTotal -
            Decimal.fromBigInt(modulo) * Decimal.fromInt(offer.value);
        print("Slice:" + slice.toString());
        if (slice < bestOffer) {
          bestOffer = slice;
        }
      }
    }

    return bestOffer;
  }

  Decimal _getPercentageTotal(Decimal total, int offerValue) {
    final percentage =
        total * Decimal.fromInt(100 - offerValue) / Decimal.fromInt(100);

    return percentage.toDecimal(scaleOnInfinitePrecision: 2);
  }
}
