import 'package:dio/dio.dart';
import 'package:edenred/network/client.dart';
import 'package:edenred/network/models/book.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

/* EVENTS */
abstract class BooksEvent {}

class GetBooks extends BooksEvent {}

/* STATES */

abstract class BooksState extends Equatable {
  const BooksState();

  @override
  List<Object?> get props => [];
}

class BooksInitial extends BooksState {}

class BooksLoading extends BooksState {}

class BooksLoaded extends BooksState {
  final List<Book> books;

  const BooksLoaded({required this.books});

  @override
  List<Object?> get props => [books];
}

class BooksError extends BooksState {}

/* BLOC */

class BooksBloc extends Bloc<BooksEvent, BooksState> {
  var books = <Book>[];

  BooksBloc() : super(BooksInitial()) {
    on<GetBooks>((event, emit) async {
      try {
        emit(BooksLoading());
        books = await _fetchBooks();
        emit(BooksLoaded(books: books));
      } catch (error) {
        emit(BooksError());
      }
    });
  }

  Future<List<Book>> _fetchBooks() async {
    final dio = Dio(); // Provide a dio instance
    final client = RestClient(dio);

    return client.getBooks();
  }
}
