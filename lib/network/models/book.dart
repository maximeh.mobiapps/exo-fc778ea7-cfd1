import 'package:decimal/decimal.dart';
import 'package:json_annotation/json_annotation.dart';

part 'book.g.dart';
/*
{
  "isbn": "cef179f2-7cbc-41d6-94ca-ecd23d9f7fd6",
    "title": "Henri Potier et le Prince de sang-mêlé",
    "price": 30,
    "cover": "https://firebasestorage.googleapis.com/v0/b/henri-potier.appspot.com/o/hp5.jpg?alt=media",
    "synopsis": [
      "Henri rentre en sixième année à l'école de sorcellerie Poudlard. Il entre alors en possession d'un livre de potion portant le mot « propriété du Prince de sang-mêlé » et commence à en savoir plus sur le sombre passé de Voldemort qui était encore connu sous le nom de Tom Jedusor."
    ]
    }
*/

@JsonSerializable()
class Book {
  final String isbn;
  final String title;
  final int price;
  final String cover;
  final List<String> synopsis;

  Book(
      {required this.isbn,
      required this.title,
      required this.price,
      required this.cover,
      required this.synopsis});

  factory Book.fromJson(Map<String, dynamic> json) => _$BookFromJson(json);
  Map<String, dynamic> toJson() => _$BookToJson(this);
}
