// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'commercial_offer.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CommercialOfferList _$CommercialOfferListFromJson(Map<String, dynamic> json) =>
    CommercialOfferList(
      offers: (json['offers'] as List<dynamic>)
          .map((e) => CommercialOffer.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$CommercialOfferListToJson(
        CommercialOfferList instance) =>
    <String, dynamic>{
      'offers': instance.offers,
    };

CommercialOffer _$CommercialOfferFromJson(Map<String, dynamic> json) =>
    CommercialOffer(
      type: $enumDecode(_$OfferTypeEnumMap, json['type']),
      sliceValue: json['sliceValue'] as int?,
      value: json['value'] as int,
    );

Map<String, dynamic> _$CommercialOfferToJson(CommercialOffer instance) =>
    <String, dynamic>{
      'type': _$OfferTypeEnumMap[instance.type],
      'sliceValue': instance.sliceValue,
      'value': instance.value,
    };

const _$OfferTypeEnumMap = {
  OfferType.percentage: 'percentage',
  OfferType.minus: 'minus',
  OfferType.slice: 'slice',
};
