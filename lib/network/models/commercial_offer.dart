import 'package:json_annotation/json_annotation.dart';

part 'commercial_offer.g.dart';
/*
{
  "offers": [
    {
      "type": "percentage",
      "value": 5
    },
    {
      "type": "minus",
      "value": 15
    },
    {
      "type": "slice",
      "sliceValue": 100,
      "value": 12
    }
  ]
}
*/

@JsonSerializable()
class CommercialOfferList {
  final List<CommercialOffer> offers;

  CommercialOfferList({required this.offers});

  factory CommercialOfferList.fromJson(Map<String, dynamic> json) =>
      _$CommercialOfferListFromJson(json);
  Map<String, dynamic> toJson() => _$CommercialOfferListToJson(this);
}

enum OfferType {
  @JsonValue('percentage')
  percentage,
  @JsonValue('minus')
  minus,
  @JsonValue('slice')
  slice
}

@JsonSerializable()
class CommercialOffer {
  final OfferType type;
  final int? sliceValue;
  final int value;

  CommercialOffer(
      {required this.type, required this.sliceValue, required this.value});

  factory CommercialOffer.fromJson(Map<String, dynamic> json) =>
      _$CommercialOfferFromJson(json);
  Map<String, dynamic> toJson() => _$CommercialOfferToJson(this);
}
