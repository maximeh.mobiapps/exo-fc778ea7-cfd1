import 'package:dio/dio.dart';
import 'package:edenred/network/models/book.dart';
import 'package:edenred/network/models/commercial_offer.dart';
import 'package:retrofit/retrofit.dart';

part 'client.g.dart';

@RestApi(baseUrl: "https://henri-potier.techx.fr/")
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  @GET("/books")
  Future<List<Book>> getBooks();

  @GET("/books/{isbns}/commercialOffers")
  Future<CommercialOfferList> getCommercialOffers(@Path("isbns") String isbns);
}
